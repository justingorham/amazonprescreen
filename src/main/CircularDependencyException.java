package main;

public class CircularDependencyException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 969913195061905275L;

	public CircularDependencyException(Package a, Package b){
		super("Circular dependency detected between " + a + " and " + b + ".");
	}
}
