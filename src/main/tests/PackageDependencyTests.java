package main.tests;

import static org.junit.Assert.*;

import java.util.List;

import main.*;

import org.junit.Test;

public class PackageDependencyTests {

	@Test
	public void goodDependency() {
		assertTrue(PackageDependency.IsValidDependency("<hello,world>"));
	}

	@Test
	public void goodDependency_ExtraSpace() {
		assertTrue(PackageDependency.IsValidDependency("     <     hello     ,    world   >    "));
	}
	
	@Test
	public void badDependency() {
		assertFalse(PackageDependency.IsValidDependency("This is just sad"));
	}
	
	@Test
	public void badDependency_TooManyCommas() {
		assertFalse(PackageDependency.IsValidDependency("<too,many,commas>"));
	}
	
	@Test
	public void badDependency_TooManyAngleBrackets() {
		assertFalse(PackageDependency.IsValidDependency("<too,manyAngleBrakets>>"));
	}
	
	@Test
	public void badDependency_TooManyAngleBrackets2() {
		assertFalse(PackageDependency.IsValidDependency("<too,manyAngleBrake>ts>"));
	}
	
	@Test
	public void createDependencyList(){
		String[] list = {
				"<A,B>",
				"<A,C>",
				"<A,D>",
				"<B,C>",
				"<B,D>",
				"<C,D>"
		};
		List<PackageDependency> dependencyList = PackageDependency.CreatePackageDependencyList(list);
		assertEquals(6,dependencyList.size());
		assertEquals(new main.Package("A"), dependencyList.get(0).primary);
		assertEquals(new main.Package("B"), dependencyList.get(0).dependency);
	}
	
	@Test
	public void createDependencyList_ABadLine(){
		String[] list = {
				"<A,B>",
				"<A,C>",
				"<A,D>",
				"<B,C>",
				"<B,D>",
				"<C,D,>"
		};
		List<PackageDependency> dependencyList = PackageDependency.CreatePackageDependencyList(list);
		assertEquals(5,dependencyList.size());
		assertEquals(new main.Package("A"), dependencyList.get(0).primary);
		assertEquals(new main.Package("B"), dependencyList.get(0).dependency);
	}
}
