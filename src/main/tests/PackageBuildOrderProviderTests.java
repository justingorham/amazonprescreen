package main.tests;

import static org.junit.Assert.*;

import java.util.*;

import main.*;
import main.Package;

import org.junit.Test;

public class PackageBuildOrderProviderTests {

	@Test
	public void detectCycle() {
		PackageBuildOrderProvider provider = new PackageBuildOrderProvider();
		Package a = new Package("a");
		Package b = new Package("b");
		Package c = new Package("c");

		List<PackageDependency> list = Arrays.asList(
				new PackageDependency(a, b), new PackageDependency(b, c),
				new PackageDependency(c, a));

		try {
			provider.buildPackageNodeTree(list);
			fail("Didn't catch the dependency cycle");
		} catch (CircularDependencyException e) {

		}
	}

	@Test
	public void exampleTest() {
		Package a = new Package("a");
		Package b = new Package("b");
		Package c = new Package("c");
		Package d = new Package("d");

		List<PackageDependency> list = Arrays.asList(
				new PackageDependency(a, b), new PackageDependency(a, c),
				new PackageDependency(a, d), new PackageDependency(b, c),
				new PackageDependency(b, d), new PackageDependency(c, d));

		PackageBuildOrderProvider provider = new PackageBuildOrderProvider();
		try {

			List<Package> output = provider.validBuildOrder(list);
			assertEquals(d, output.get(0));
			assertEquals(c, output.get(1));
			assertEquals(b, output.get(2));
			assertEquals(a, output.get(3));
		} catch (CircularDependencyException e) {
			fail(e.getMessage());
		}
	}

}
