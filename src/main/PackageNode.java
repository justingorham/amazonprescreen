package main;

import java.util.*;

public class PackageNode {
	public Package nodePackage;
	
	public HashMap<Package, PackageNode> childDependencies;
	
	public PackageNode(Package nodePackage){
		this.nodePackage = nodePackage;
		childDependencies = new HashMap<Package, PackageNode>();
	}

	public boolean isLeaf(){
		return childDependencies.size() == 0;
	}
	
	public boolean Visited;
}
