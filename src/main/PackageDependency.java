package main;

import java.util.*;
import java.util.regex.Pattern;

public class PackageDependency {
	public PackageDependency(){}
	public PackageDependency(Package primary, Package dependency){
		this.primary = primary;
		this.dependency = dependency;
	}
	
	public Package primary;
	public Package dependency;
	
	private static String dependencyPattern = "^\\s*<\\s*[^\\s,<>]+\\s*,\\s*[^\\s,<>]+\\s*>\\s*$";
	
	public static boolean IsValidDependency(String dependencyString){
		return Pattern.matches(dependencyPattern, dependencyString);
	}
	
	public static List<PackageDependency> CreatePackageDependencyList(String[] Dependencies){
		return CreatePackageDependencyList(Arrays.asList(Dependencies));
	}
	
	public static List<PackageDependency> CreatePackageDependencyList(List<String> Dependencies){
		List<PackageDependency>  returnList = new ArrayList<PackageDependency>();
		HashMap<String, Package> packages = new HashMap<String, Package>();
		for(String d : Dependencies){
			if(!IsValidDependency(d)) continue;
			String packageNames = d.substring(d.indexOf('<')+1, d.indexOf('>'));
			String[] packageNameArray = packageNames.split(",");
			packageNameArray[0] = packageNameArray[0].trim();
			packageNameArray[1] = packageNameArray[1].trim(); 
			
			//Get (or generate) the primary package
			Package primary = packages.get(packageNameArray[0]);
			if(primary == null){
				primary = new Package(packageNameArray[0]);
				packages.put(packageNameArray[0], primary);
			}
			
			//Get (or generate) the dependency package
			Package dependency = packages.get(packageNameArray[1]);
			if(dependency == null){
				dependency = new Package(packageNameArray[1]);
				packages.put(packageNameArray[1], dependency);
			}
			
			returnList.add(new PackageDependency(primary, dependency));
		}
		return returnList;
	}
	
}
