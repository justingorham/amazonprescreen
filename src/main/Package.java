package main;

public class Package {
	public String packageName;
	
	public Package(){}
	
	public Package(String packageName){
		this.packageName = packageName;
	}
	
	@Override
	public boolean equals(Object o){
		if(o == this)
			return true;
		
		if(!(o instanceof Package))
			return false;
		
		Package obj = (Package) o;
		return obj.packageName.equals(this.packageName);
	}
	
	@Override
	public int hashCode(){
		return packageName.hashCode();
	}
	
	@Override
	public String toString(){
		return packageName;
	}
}
