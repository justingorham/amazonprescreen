package main;

import java.io.*;
import java.util.*;

public class AppStart {
	public static void main(String[] args) {
		if(args.length == 0){
			System.err.println("No input file was specified. Please specify an input file as the first argument");
			System.exit(0);
		}
		
		String filePathString = args[0];
		File f = new File(filePathString);
		if(!f.exists() || f.isDirectory()){
			System.err.println(filePathString + " is not an existing file.");
			System.exit(0);
		}
		List<String> dependencyLines = new ArrayList<String>();
		
		try {
			Scanner scanner = new Scanner(f);
			while(scanner.hasNextLine()){
				dependencyLines.add(scanner.nextLine());
			}
			scanner.close(); //clean up, clean up. Everybody, everywhere.
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			System.exit(0);
		}
		
		List<PackageDependency> dependencyList = PackageDependency.CreatePackageDependencyList(dependencyLines);
		List<Package> orderedPackages = null;
		try {
			orderedPackages = new PackageBuildOrderProvider().validBuildOrder(dependencyList);
		} catch (CircularDependencyException e) {
			System.err.println(e.getMessage());
			System.exit(0);
		}
		
		System.out.println("Build Order");
		System.out.println("");
		for(Package p : orderedPackages){
			System.out.println(p);
		}
		System.out.println("");
		
		String fileName = args.length >= 2 ? args[1].trim() : "BuildOrder.txt";
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(fileName);
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			System.exit(0);
		}
		for(Package p : orderedPackages){
			writer.println(p);
		}
		writer.close();
		System.out.println("Output written to "+ new File(fileName).getAbsolutePath());
	}
	
	
}
