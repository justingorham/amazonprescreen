package main;

import java.util.*;

public class PackageBuildOrderProvider {
	public List<Package> validBuildOrder(
			List<PackageDependency> packageDependencies) throws CircularDependencyException {
		PackageNode head = buildPackageNodeTree(packageDependencies);
		List<Package> list = new ArrayList<Package>();
		EnqueuePackages(head, list);
		return list;
	}

	public PackageNode buildPackageNodeTree(
			List<PackageDependency> packageDependencies) throws CircularDependencyException {
		PackageNode head = new PackageNode(null);
		for (PackageDependency p : packageDependencies) {
			PackageNode primary = head.childDependencies.get(p.primary);
			if(primary == null){
				primary = new PackageNode(p.primary);
				head.childDependencies.put(primary.nodePackage, primary);
			}
			PackageNode dependent = head.childDependencies.get(p.dependency);
			if(dependent == null){
				dependent = new PackageNode(p.dependency);
				head.childDependencies.put(dependent.nodePackage, dependent);
			}
			if(findPackage(dependent, primary.nodePackage)!=null) throw new CircularDependencyException(primary.nodePackage, dependent.nodePackage);
			primary.childDependencies.put(dependent.nodePackage, dependent);
		}
		return head;
	}
	
	public PackageNode findPackage(PackageNode current, Package p){
		List<PackageNode> visited = new ArrayList<PackageNode>();
		PackageNode pNode = findPackage(current, p, visited);
		for(PackageNode v : visited){
			v.Visited = false;
		}
		return pNode;
	}

	private PackageNode findPackage(PackageNode current, Package p, List<PackageNode> visited) {
		if (current.nodePackage.equals(p))
			return current;
		if (current.Visited || current.isLeaf())
			return null;
		PackageNode child = current.childDependencies.get(p);
		if(child != null) return child;
		current.Visited = true;
		visited.add(current);
		for (PackageNode pack : current.childDependencies.values()) {
			PackageNode next = findPackage(pack, p, visited); 
			if (next != null)
				return next;
		}
		return null;
	}
	
	public void EnqueuePackages(PackageNode current, List<Package> queue){
		if(current.Visited) return;
		for(PackageNode p : current.childDependencies.values()){
			EnqueuePackages(p, queue);
		}
		if(current.nodePackage != null) //Don't add null packages. The head node should be the only node that contains a null package.
			queue.add(current.nodePackage);
		current.Visited = true;
	}
}
